package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class KennyChesney {
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public KennyChesney() {
    }
    
    public ArrayList<Song> getBeatlesSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("There goes my life", "Kenny Chesney");             //Create a song
         Song track2 = new Song("I Go Back", "Kenny Chesney");         //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for George Strait
         this.albumTracks.add(track2);                                          //Add the second song to song list for George Strait 
         return albumTracks;                                                    //Return the songs for the Beatles in the form of an ArrayList
    }
}

