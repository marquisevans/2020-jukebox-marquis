package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class RobertRandolph {

	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public RobertRandolph() {
    }
    
    public ArrayList<Song> getRobertRandolphSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                           //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Lacrimosa", "Robert Randolph");         				//Create a song
         Song track2 = new Song("Dies Irae", "Robert Randolph");        //Create another song
         Song track3 = new Song("Good News", "Robert Randolph");       //Create a third song
         this.albumTracks.add(track1);                                  //Add the first song to song list
         this.albumTracks.add(track2);                                  //Add the second song to song list
         this.albumTracks.add(track3);                                  //Add the third song to song list
         return albumTracks;                                            //Return the songs for Robert Randolph in the form of an ArrayList
    }
}