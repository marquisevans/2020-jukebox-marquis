package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class Tool {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Tool() {
    }
    
    public ArrayList<Song> getToolSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Schism", "Tool");             					//Create a song
         Song track2 = new Song("Forty Six and Two", "Tool");        		 	//Create another song
         Song track3 = new Song("The Pot", "Tool");         					//Create another song         
         this.albumTracks.add(track1);                                          //Add the first song to song list for Tool
         this.albumTracks.add(track2);                                          //Add the second song to song list for Tool
         this.albumTracks.add(track3);                                          //Add the second song to song list for the Tool         
         return albumTracks;                                                    //Return the songs for the Beatles in the form of an ArrayList
    }
}
